Router.configure({
    layoutTemplate: 'ApplicationLayout'
});

Router.route('/', function() {
    this.render('home');
}, {
    name: 'home'
});

Router.route('/game/:_id', function(){
    this.render('game', {
        data: function () {
            var gameId = this.params._id,
                game = Games.findOne(gameId);
            
            var opponentIndex, 
                currentUserIndex;
            
            if (game.players[0].player === Meteor.userId()){
                currentUserIndex = 0;
                opponentIndex = 1;
            }else{
                currentUserIndex = 1;
                opponentIndex = 0;
            }
            
            Session.set('opponentIndex', opponentIndex);
            Session.set('currentUserIndex', currentUserIndex);
            
            return game;
        }
    }) ;
}, {
    name: 'game'
});

Router.route('/search', function (){
    this.render('search');
});