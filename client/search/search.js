Template.search.helpers({
    friended: function () {
        if(Meteor.user().profile) return Meteor.user().profile.friends.indexOf(this._id) + 1;
    },
    isYou: function () {
        return Meteor.userId() === this._id;
    },
    userIndex: () => UsersIndex
});

Template.search.events({
    "click .add-friend": function() {
        Meteor.users.update(Meteor.userID(), {
            $addToSet: {
                "profile.friends": this._id
            }
        });
    }
});