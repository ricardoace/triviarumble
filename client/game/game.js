function Trivia(args){
    args = args || {};
    this.question = args.question;
    this.choices = args.choices;
    this.correctAnswer = args.correctAnswer;   
}

function randomQuestion() {
    var ref = backendless.Persistence.of(Trivia);
    
    var query = new backendless.DataQuery();
    query.options = {
        page: 51
    };

    var trivia = ref.find(query);
    
    var randomQuestions = _.sample(trivia.data);
    
    randomQuestions.choices = JSON.parse(randomQuestions.choices);
    Session.set('activeQuestion', randomQuestions);
}

Template.game.onRendered(function (){
    randomQuestion();
});

Template.game.helpers({
    opponentIndex: function () {
        return Session.get('opponentIndex'); 
    },
    currentUserIndex: function (){
        return Session.get('currentUserIndex');
    },
    findScore: function(user){
        if(typeof this !== 'undefined'){
            return this.playera[user].score;
        }
    },
    
    activeQuestion : function(){
        return Session.get('activeQuestion');
    }
});
Template.game.events({
    "click .pick": function (){
        var question = Session.get('activeQuestion'),
            choiceIndex = question.choices.indexOf(this.valueOf());
        
        if(choiceIndex === question.correctAnswer) {
          var game = Games.findOne(UI.getData().id);
            
            if(Session.get('currentUserIndex') === 0) {
                Games.update(UI.getData()._id, {
                    $inc: {
                        "players.0.score": 1, 
                        round: 1
                    }
                }
                )};      
            if(game.players[0].score >= 9) {
                Game.update(UI.getData()._id, {
                    $set: {
                        winner: Meteor.userId(),
                        gameStatus: "finished"
                    }
                });
              }else{
                    randomQuestion();
              }
                             
           } else if(Session.get('currentUserIndex') === 1) {
                Games.update(UI.getData()._id, {
                    $inc: {
                        "players.0.score": 1, 
                        round: 1
                    }
                }
                )};      
            if(game.players[1].score >= 9) {
                Game.update(UI.getData()._id, {
                    $set: {
                        winner: Meteor.userId(),
                        gameStatus: "finished"
                    }
                });
              }else{
                    randomQuestion();
              }
                             
           }
        } else{
                     if(Session.get('currentUserIndex') === 0){
                    Games.update(UI.getData()._id, {
                        $set: {
                            turn: UI.getData().players[1].player
                        },
                        $inc: {
                            round: 1
                        }
                    });    
            } else if(Session.get('currentUserIndex') === 1){
                    Games.update(UI.getData()._id, {
                        $set: {
                            turn: UI.getData().players[0].player
                        },
                        $inc: {
                            round: 1
                        }
                    });    
            }
        }
     }
});































